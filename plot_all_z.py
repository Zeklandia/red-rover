# -*- coding: utf-8 -*-
# Written by: Asher Wood (zeklandia)

import red_rover as rover
import send_over

targets = send_over.load_targets_from_file()

rover.fetch_OAC_z(targets=targets)

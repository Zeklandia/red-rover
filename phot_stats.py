# -*- coding: utf-8 -*-
# Written by: Asher Wood (zeklandia)

import glob
from os import path, getcwd

import numpy as np
from astropy.io import ascii
from astropy.table import Table, MaskedColumn
from tqdm import tqdm

# find photometry files and load them
filelist = glob.glob("data/OAC/photometry/sne_*.csv")

pbar0 = tqdm(desc="Loading files", total=len(filelist),
             unit=" files", leave=False)
snes = []
for file in filelist:
    pbar0.write("Reading " + file + "...")
    snes.append(ascii.read(file))
    pbar0.update(1)
pbar0.close()

# identify bands
bands = ["g", "r", "i", "z", "B", "V", "R"]

pbar1 = tqdm(desc="Identifying bands", total=len(filelist),
             unit=" files", leave=True)
pbar1.write("Scanning for bands...")
for sne in snes:
    # replace blanks with null, but only if there were blanks
    for band in np.unique(sne["band"].filled(None) if isinstance(sne["band"], type(MaskedColumn())) else sne["band"]):
        if band not in bands:
            bands.append(band)
    pbar1.update(1)
pbar1.close()

# track statistics
# The list comprehension acrobatics are to ignore what used to be blank values.
# astropy can't bear the weight of uncertainty, so data types must be specified
# in advance. To that end, "S256" should mean a 256-character string, to give
# enough room for event names
stats = Table(names=("event", *[band for band in bands if band is not None]),
              dtype=("S64", *["int_" for band in bands if band is not None]))

pbar2 = tqdm(desc="Processing files", total=len(filelist),
             unit=" files", leave=True)
for sne in snes:
    events = np.unique(sne["event"])

    pbar3 = tqdm(desc="Processing events", total=len(events),
                 unit="events", leave=False)

    for event in events:
        pbar3.write("Processing " + event + "...")
        # count data points in each band
        _bands, _counts = np.unique(sne["band"].filled(None) if isinstance(
            sne["band"], type(MaskedColumn())) else sne["band"], return_counts=True)
        counts = dict(zip(_bands, _counts))
        pbar3.update(1)

        # fill in missing bands with 0
        for band in bands:
            if band not in counts.keys():
                counts[band] = 0

        # add counting statistics to table
        stats.add_row([event, *[counts[band] for band in bands if band is not None]])
    pbar3.close()
    pbar2.update(1)
pbar2.close()

# save data
data_file = "phot_stats.csv"
data_file = path.join(getcwd(), path.normpath(data_file))
ascii.write(stats, data_file, format="csv", overwrite=True)

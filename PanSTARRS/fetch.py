import os

import requests
from astropy.io.votable import parse_single_table

data_dir = os.path.join(os.getcwd(), os.path.normpath("data/"))

def fetch(ra_hms, dec_dms, radius_arcmin, as_array=True, equinox='J2000', coordformat='sex', data_dir=data_dir):
    """
    Returns an array (or by choice, an AstroPy table) of data from the PanSTARRS catalog and saves the data.

    Args:
        ra_hms (string):       right acension for the center of the target field in 'HH MM SS.SS'
        dec_dms (string):      declination for the center of the target field in 'DD MM SS.SS'
        radius_arcmin (float): radius of the target field in arc minutes
        as_array (boolean):    whether, or not, to return the data as an array, or as an AstroPy table

    Returns:
        data (array or astropy.io.votable): objects, their positions, and assorted photometric data
    """

    url = 'http://archive.stsci.edu/panstarrs/search.php'

    print('Checking existing data...')

    # generate filename based on paramters to avoid overwriting data not included in current fetch
    file_name = "/PanSTARRS/ra_{ra}.dec_{dec}.radius_{r}.xml".format(ra=str(ra_hms).replace(
        ' ', '+'), dec=str(dec_dms).replace(' ', '+'), r=str(radius_arcmin).replace('.', '-'))

    # set path for data file
    data_file = os.path.join(os.getcwd(), os.path.normpath(data_dir + file_name))

    # skip fetch if data already exists
    if not os.path.isfile(data_file):
        print('Downloading PanSTARRS data...')

        # fetch data
        r = requests.get(url, params={'action': 'Search',
                                      'ra': str(ra_hms),
                                      'dec': str(dec_dms),
                                      'radius': str(radius_arcmin),
                                      'equinox': equinox,
                                      'resolver': 'Resolve',
                                      'nDetections': '> 1',
                                      'ordercolumn1': 'ang_sep',
                                      'ordercolumn2': 'objid',
                                      'ordercolumn3': 'null',
                                      'coordformat': coordformat,
                                      'outputformat': 'VOTable',
                                      'max_records': '10000',
                                      'max_rpp': '5000',
                                      'selectedColumnsCsv': 'objname,ramean,decmean,gmeanpsfmag,gmeanpsfmagerr,gmeanpsfmagstd,rmeanpsfmag,rmeanpsfmagerr,rmeanpsfmagstd,imeanpsfmag,imeanpsfmagerr,imeanpsfmagstd,zmeanpsfmag,zmeanpsfmagerr,zmeanpsfmagstd,ymeanpsfmag,ymeanpsfmagerr,ymeanpsfmagstd,ang_sep,qualityflag'})

        # save data
        print('Saving data...')
        file_write = open(data_file, 'w')
        file_write.write(r.text)
        file_write.close()
        print('Data saved.')
    else:
        print('Existing data found.')

    # parse as AstroPy data table
    print('Parsing data...')
    data = parse_single_table(data_file)

    # return data in the requested format
    if as_array:
        data = data.array
    else:
        data = data.to_table(use_names_over_ids=True)

    return data

# -*- coding: utf-8 -*-
# Written by: Asher Wood (zeklandia)

import os

import OAC.mwebv.fetch as oac_mwebv
import OAC.photometry.fetch as oac_phot
import OAC.redshift.fetch as oac_z
import OAC.spectra.fetch as oac_spec
import PanSTARRS.fetch as panstarrs

data_dir = os.path.join(os.getcwd(), os.path.normpath("data/"))
targets_dir = os.getcwd()


def fetch_PanSTARRS(ra_dms, dec_dms, radius_arcmin=3.0, as_array=True, data_dir=data_dir):
    '''
    Runs a PanSTARRS object and photometry fetch

    Args:
        ra_hms (string):       right acension for the center of the target field in 'HH MM SS.SS'
        dec_dms (string):      declination for the center of the target field in 'DD MM SS.SS'
        radius_arcmin (float): radius of the target field in arc minutes
        as_array (boolean):    whether, or not, to return the data as an array, or as an AstroPy table

    Returns:
        data (array or astropy.io.votable): objects, their positions, and assorted photometric data
    '''
    data = panstarrs.fetch(ra_dms, dec_dms, radius_arcmin, as_array, data_dir)
    return data


def fetch_OAC_spec(targets, data_dir=data_dir):
    """
    Runs an Open Astronomy Catalogs spectra fetch
    TODO:
     - get list of dates and number of readings available for each supernova
     - get spectrum for specific date and reading
    """
    data = oac_spec.fetch(targets, data_dir)
    return data


def fetch_OAC_phot(targets, bands=["Y"], data_dir=data_dir):
    '''
    Runs an Open Astronomy Catalogs photometric data fetch
    '''
    data = oac_phot.fetch(targets, bands, data_dir)
    return data


def fetch_OAC_z(targets, data_dir=data_dir):
    '''
    Runs an Open Astronomy Catalogs z (redshift) data fetch
    '''
    data = oac_z.fetch(targets, data_dir)
    return data


def fetch_OAC_mwebv(targets, data_dir=data_dir):
    '''
    Runs an Open Astronomy Catalogs z (redshift) data fetch
    '''
    data = oac_mwebv.fetch(targets, data_dir)
    return data

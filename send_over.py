# -*- coding: utf-8 -*-
# Written by: Asher Wood (zeklandia)

import csv
import os

targets_file = os.path.join(os.getcwd(), os.path.normpath("targets.csv"))
bands_file = os.path.join(os.getcwd(), os.path.normpath("bands.csv"))


def load_targets_from_file(arg_targets_file=targets_file):
    """
    Reads a CSV file and returns a list of objects

    Args:
        arg_targets_file (path): CSV file of objects

    Returns:
        targets (list): objects from targets_file
    """
    with open(arg_targets_file, 'r') as file_read:
        reader = csv.reader(file_read, quoting=csv.QUOTE_NONE)
        targets = [item for line in reader for item in line]
    return targets


def load_bands_from_file(arg_bands_file=bands_file):
    """
    Reads a CSV file and returns a list of photometric bands

    Args:
        arg_bands_file (path): CSV file of photometric bands

    Returns:
        bands (list): photometric bands from bands_file
    """
    with open(arg_bands_file, 'r') as file_read:
        reader = csv.reader(file_read, quoting=csv.QUOTE_NONE)
        bands = [item for line in reader for item in line]
    return bands

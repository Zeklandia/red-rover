# -*- coding: utf-8 -*-
# Written by: Asher Wood (zeklandia)

import red_rover as rover
import send_over

targets = send_over.load_targets_from_file()
rover.fetch_OAC_spec(targets)

# incomplete plotting routine
"""
# add silicon line
plt.axvline(x=6150.0, color="gray", linestyle=':', label="Si Ⅱ")
plt.legend(loc="upper left")

# add finer details to plot
plt.title(sn + str(event[0]))
plt.xlabel("Wavelength (Å)")
plt.ylabel("Flux")
plt.savefig(os.path.join(os.getcwd(), os.path.normpath(
    "data/" + "SNe." + sn + ".t0_" + str(event).replace('.', '-'))))
"""

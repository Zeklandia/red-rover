# -*- coding: utf-8 -*-
# Written by: Asher Wood (zeklandia)

import os
import sys
from contextlib import redirect_stdout

import io
import re
from astropy.io import ascii
from astropy.table import Table, vstack
from astroquery.oac import OAC
from tqdm import tqdm

data_dir = os.path.join(os.getcwd(), os.path.normpath("data/"))


def fetch(targets, data_dir=data_dir):
    """
    Fetch OAC data
    """

    # can't work without any targets
    if targets is None:
        print("No targets!", file=sys.stderr)
        sys.exit(1)

    pbar = tqdm(desc="Loading data", total=len(targets),
                unit=" SNe", leave=False)

    # format as astropy table
    ebvs = Table(names=("event", "mwebv"), dtype=("str", "str"))

    for target in targets:
        # download Milky Way E(B-V) values
        pbar.write("Downloading " + target + "...")
        oac_stdout = io.StringIO()
        with redirect_stdout(oac_stdout):
            data = OAC.query_object(target, quantity="ebv", argument="first")
        output = oac_stdout.getvalue()

        # check if the data returned is an error message
        if re.search("error", output, re.IGNORECASE):
            pbar.write("    Error!")
            pbar.write("    " + output)
            pbar.update(1)
            continue
        pbar.write("    Download complete.")

        if len(data["ebv"][0]) > 0:
            ebvs = vstack(tables=[ebvs, data], metadata_conflicts="silent")
        else:
            pbar.write("    Skipping empty mwebv data.")

        pbar.write("    Data collected.")
        pbar.update(1)

    pbar.write("Processing mwebv data...")

    # save data
    data_file = "/OAC/mwebv/mwebv.csv"
    data_file = os.path.join(os.getcwd(), os.path.normpath(data_dir + data_file))
    if not os.path.isfile(data_file):
        pbar.write("Saving data...")
        ascii.write(ebvs, data_file, format="csv")
    else:
        pbar.write("Local data found. New data not saved!")
    pbar.close()

# -*- coding: utf-8 -*-
# Written by: Asher Wood (zeklandia)

import os
import sys
from contextlib import redirect_stdout

import io
import re
from astropy.io import ascii
from astroquery.oac import OAC
from tqdm import tqdm

data_dir = os.path.join(os.getcwd(), os.path.normpath("data/"))


def fetch(targets, bands, data_dir=data_dir):
    """
    Fetch OAC data
    """

    if targets is None:
        print("No targets!", file=sys.stderr)
        sys.exit(1)

    pbar = tqdm(desc="Loading data", total=len(targets),
                unit=" SNe", leave=False)
    for target in targets:
        target = str(target)
        pbar.write("Downloading " + target + " data...")

        # download photometry
        try:
            oac_stdout = io.StringIO()
            with redirect_stdout(oac_stdout):
                if bands:
                    data = OAC.get_photometry(target, argument="band=" + str(bands))
                else:
                    data = OAC.get_photometry(target)
            output = oac_stdout.getvalue()

            # check if the data returned is an error message
            if re.search("error", output, re.IGNORECASE):
                pbar.write("    Error!")
                pbar.write("    " + output)
                pbar.update(1)
                continue
        except IndexError:
            pbar.write("    No data!")
            pbar.update(1)
            continue
        pbar.write("    Download complete.")

        if len(data) > 0:
            # save data
            bands_str = ""
            if len(bands) > 1:
                for band in bands[:-1]:
                    bands_str = bands_str + band + " "
                bands_str = bands_str + bands[-1]
            elif len(bands) == 1:
                bands_str = bands[0]
            else:
                bands_str = "any"
            data_file = "/OAC/photometry/sne_{target}.bands_{bands}.csv".format(
                target=target.replace(':', '-'), bands=bands_str.replace(' ', '-'))
            data_file = os.path.join(os.getcwd(), os.path.normpath(data_dir + data_file))
            if not os.path.isfile(data_file):
                ascii.write(data, data_file, format="csv")
                pbar.write("    Data saved.")
            else:
                pbar.write("    Local data found.")

        else:
            pbar.write("Skipping empty photometry data (" + target + "; " + bands_str + " bands).")
        pbar.write("   Done.")
        pbar.update(1)
    pbar.close()

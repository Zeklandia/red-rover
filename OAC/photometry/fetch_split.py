# -*- coding: utf-8 -*-
# Written by: Asher Wood (zeklandia)

import os
import sys

from astropy.io import ascii
from astroquery.oac import OAC
from tqdm import tqdm

data_dir = os.path.join(os.getcwd(), os.path.normpath("data/"))


def fetch(targets, bands=["Y"], data_dir=data_dir):
    """
    Fetch OAC data
    """

    # can't work without any targets
    if targets is None:
        print("No targets!", file=sys.stderr)
        sys.exit(1)

    pbar = tqdm(desc="Loading data", total=len(targets),
                unit=" SNe", leave=False)
    for target in targets:
        pbar.write("Downloading " + target + " data...")
        pbar2 = tqdm(desc="Loading " + target, total=len(bands),
                     unit=" bands", leave=False)
        for band in bands:
            pbar2.write("    Downloading " + band + " band data...")
            # download photometry
            try:
                data = OAC.get_photometry(target, argument="band=" + band)
            except IndexError:
                pbar2.write("        No data in the " + band + " band!")
                pbar2.update(1)
                continue
            pbar2.write("        Download complete.")

            if len(data) > 0:
                pbar2.write("    Saving " + band + " band data...")

                # save data
                data_file = "/OAC/photometry/sne_{target}.band_{band}.csv".format(
                    target=target.replace(':', '-'), band=band)
                data_file = os.path.join(os.getcwd(), os.path.normpath(data_dir + data_file))
                if not os.path.isfile(data_file):
                    ascii.write(data, data_file, format="csv")
                    pbar2.write("        Saved.")
                else:
                    pbar2.write("        Local data found.")

            else:
                pbar2.write("        Skipping empty photometry data (" +
                            target + ", " + band + " band).")
            pbar2.write("        Done.")
            pbar2.update(1)
        pbar2.close()
        pbar.write("   Done.")
        pbar.update(1)
    pbar.close()

# -*- coding: utf-8 -*-
# Written by: Asher Wood (zeklandia)

import csv
import os
import sys

import numpy as np
from astropy.io import ascii
from astropy.table import Table
from astroquery.oac import OAC
from tqdm import tqdm

data_dir = os.path.join(os.getcwd(), os.path.normpath("data/"))


def fetch(targets, data_dir=data_dir):
    """
    Fetch OAC data for supernova(e)

    targets (str/list):   supernova(e) whose data to obtain
    """

    data = Table()

    # can"t work without any targets
    if targets is None:
        targets_file = os.path.join(os.getcwd(), os.path.normpath("targets.csv"))
        with open(targets_file, 'r') as file_read:
            reader = csv.reader(file_read, delimiter=',')
            targets = np.asarray(list(reader)).flatten()
        if targets is None:
            print("No targets!", file=sys.stderr)
            sys.exit(1)

    # track progress through targets
    pbar = tqdm(desc="Loading data", total=len(targets),
                unit=" SNe", leave=False)

    for target in targets:
        pbar.write("Processing " + target + "...")
        readings = Table()
        spectrum = Table()
        try:
            # download list of events for the target
            readings = OAC.query_object(target, "spectra", "time")["time"]
        except IndexError:
            # AstroQuery crashes if the list is empty
            pbar.write("    No spectra found.")
            pbar.update(1)

        pbar2 = tqdm(desc="Loading spectra", total=len(readings), unit=" spectra", leave=False)
        for date in readings:
            file_name = "/OAC/spectra/sne_{target}.t_{t}.csv".format(
                target=target.replace(':', '-'), t=str(date).replace('.', '-'))
            data_file = os.path.join(os.getcwd(), os.path.normpath(data_dir + file_name))

            if not os.path.isfile(data_file):
                pbar2.write("    Downloading " + target + " date " + date + "...")

                try:
                    # download data for a single spectrum
                    spectrum = OAC.get_single_spectrum(target, time=date)
                except IndexError:
                    # AstroQuery will crash if the data is empty
                    pbar2.write("        Skipping empty spectra.")
                    pbar2.update(1)

                # save data
                ascii.write(spectrum, data_file)

                pbar2.write("        Done.")
                pbar2.update(1)
            else:
                pbar2.write("Skipping " + target + " reading " + date + ", local data found.")
                spectrum = ascii.read(data_file, format="csv")

        data = {**data, **spectrum}
        pbar.write("    Done.")
        pbar.update(1)
    pbar.close()
    return data

# -*- coding: utf-8 -*-
# Written by: Asher Wood (zeklandia)

import os
import sys
from contextlib import redirect_stdout

import io
import numpy as np
import re
from astropy.io import ascii
from astropy.table import Table
from astroquery.oac import OAC
from tqdm import tqdm

data_dir = os.path.join(os.getcwd(), os.path.normpath("data/"))


def fetch(targets, data_dir=data_dir):
    """
    Fetch OAC data

    Args:
        targets: list of objects for which to fetch data
    """

    # can't work without any targets
    if targets is None:
        print("No targets!", file=sys.stderr)
        sys.exit(1)

    pbar = tqdm(desc="Loading data", total=len(targets),
                unit=" SNe", leave=False)
    for target in targets:
        # download spectra
        pbar.write("Downloading " + target + "...")
        oac_stdout = io.StringIO()
        with redirect_stdout(oac_stdout):
            data = OAC.get_spectra(target)
        output = oac_stdout.getvalue()
        if re.search("error", output, re.IGNORECASE):
            pbar.write("    Error!")
            pbar.write("    " + output)
            pbar.update(1)
            continue
        pbar.write("    Download complete.")

        # extract only the spectra
        spectra = data.get(target).get("spectra")

        if spectra != []:
            pbar.write("    Processing spectra...")
            # separate readings
            pbar2 = tqdm(desc="Loading spectra", total=len(spectra), unit=" spectra", leave=False)

            # track multiple spectra for a single day
            last = ""
            count = 0
            for spectrum in spectra:
                date = spectrum[0]

                if str(date) == last:
                    pbar2.write("        Loading spectrum " +
                                str(count + 2) + " from " + str(date) + "...")

                    # prevent 3-column data from sneaking in
                    spectrum = np.array(spectrum[1])[:, 0:2]
                    spectrum = Table(rows=spectrum, names=("wavelength", "flux"))

                    # save data
                    data_file = "/OAC/spectra/sne_{target}.t_{t}.{count}.csv".format(
                        target=target, t=date.replace('.', '-'), count=count)
                    count += 1
                else:
                    count = 0
                    pbar2.write("        Loading spectrum from " + str(date) + "...")

                    # prevent 3-column data from sneaking in
                    spectrum = np.array(spectrum[1])[:, 0:2]
                    spectrum = Table(rows=spectrum, names=("wavelength", "flux"))

                    # save data
                    data_file = "/OAC/spectra/sne_{target}.t_{t}.csv".format(
                        target=target.replace(':', '-'), t=date.replace('.', '-'))
                data_file = os.path.join(os.getcwd(), os.path.normpath(data_dir + data_file))

                if not os.path.isfile(data_file):
                    ascii.write(spectrum, data_file, format="csv")
                else:
                    pbar2.write("        Local data found.")
                pbar2.write("        Done.")
                pbar2.update(1)
                last = str(date)
            pbar2.close()
        else:
            pbar.write("Skipping empty spectra data (" + target + ").")
        pbar.write("    Done.")
        pbar.update(1)
    pbar.close()
